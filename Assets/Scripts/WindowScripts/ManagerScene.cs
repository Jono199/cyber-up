﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class ManagerScene : MonoBehaviour {
    

    //whatever number that is specified will open that scene
    public void loadScene(int sceneNum)
    {
        SceneManager.LoadScene(sceneNum);
    }
}
