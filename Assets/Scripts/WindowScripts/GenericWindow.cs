﻿using UnityEngine.EventSystems;
using UnityEngine;

public class GenericWindow : MonoBehaviour {

    public GameObject firstSelected;
    public static WindowManager manager;    //to hold a reference to the windowManager class for toggling between windows

    protected EventSystem eventSystem
    {
        get
        {
            return GameObject.Find("EventSystem").GetComponent<EventSystem>();  //get the reference of the event system which then can be used here
        }
    }

    //to highlight whichever object that is specified in the object reference: firstSelected
    public virtual void highLightFirstSelected()
    {
        eventSystem.SetSelectedGameObject(firstSelected);
    }

    //be able to control whether the window can be visible or not
    public virtual void display(bool visibility)
    {
        gameObject.SetActive(visibility);
    }

    //called when opening the scene and window
    public virtual void open()
    {
        display(true);
        highLightFirstSelected();
    }

    //Closing the window
    public virtual void close()
    {
        display(false);
    }

    //virtual will allow classes that extends this class to be overwritten
    //will close the window default
    protected virtual void Awake()
    {
        close();
    }
}
