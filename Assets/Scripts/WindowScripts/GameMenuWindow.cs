﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenuWindow : MonoBehaviour {

    public GameObject menuScene;
    public GameObject nextScene;

    public void vivewNextScene()
    {
        menuScene.SetActive(false);
        nextScene.SetActive(true);
    }

}
