﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class WindowManager : MonoBehaviour {

    public GenericWindow[] windows;
    public int currentWinID;
    public int defaultWinID;

    //to get the window itself
    public GenericWindow getWindow(int value)
    {
        return windows[value];
    }

    //method to test if the right window is being opened
    public void toggleWin(int windowValue)
    {
        int totalWindow = windows.Length;

        for(int i = 0; i < totalWindow; i++)
        {
            var window = windows[i];

            if(i == windowValue)    //Condition to open only the window that is needed
            {
                window.open();
            }
            else if (window.gameObject.activeSelf)  //see if the window is open but not the right one that we specify
            {
                window.close();
            }
        }
    }

    //Open the window after testing to see if it is correct one
    public GenericWindow Open(int value)
    {
        //test if there any windows that exist
        if(value < 0 || value >= windows.Length)
            return null;

        //if there is then save this value
        currentWinID = value;
        toggleWin(currentWinID);
        return this.getWindow(currentWinID);
    }

    //function to toggle between scenes
    public void loadScene(int sceneNum)
    {
        SceneManager.LoadScene(sceneNum);
    }

    private void Start()
    {
        GenericWindow.manager = this;   //now all generic window can talk directly to the window manager
        Open(defaultWinID);
    }
}
