﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverWindow : GenericWindow {

    public Text username;
    public Text highscore;
    public Text current_score;
    public score_tracker trackedScore;


    protected override void Awake()
    {
        resetText();
        this.open();
    }
    private void FixedUpdate()
    {
        username.text = PlayerPrefs.GetString("Player").ToString();
        highscore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
        current_score.text = trackedScore.score.ToString();
    }

    public override void open()
    {
        resetText();
        base.open();
    }
    
    public void resetText()
    {
        username.text = "";
        highscore.text = "";
    }

    public void retry(int scene)
    {
        trackedScore.reset();
        manager.Open(scene);
    }
}
