﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionWindow : GenericWindow {

    protected override void Awake()
    {
        this.open();
    }

    public void start_game()
    {
        manager.Open(1);
    }
}
