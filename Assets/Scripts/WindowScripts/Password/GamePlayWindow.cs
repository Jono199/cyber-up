﻿using UnityEngine.UI;
using UnityEngine;

public class GamePlayWindow : GenericWindow {

    public Text displayedText;  //hold the text being diplayed to the user to decide if it is a weak - strong password
    public int scoreAmount = 10;    //the amount of score awarded
    public score_tracker tracked_score; //hold the reference for the score_tracker
    public read_password passwords; //hold the reference to the read_password class to access the text file
    public static System.Random rand = new System.Random(); //randomise array position so that it shows different passwords in the password field

    protected override void Awake()
    {
        open();
        tracked_score.reset();
        nextPassword();
    }

    private void Update()
    {
        if (tracked_score.getLives() <= 0)
        {
            manager.Open(2);
        }
    }

    private void OnDisable()
    {
        if(PlayerPrefs.GetInt("HighScore",0) < tracked_score.score)
        {
            PlayerPrefs.SetString("Player", "Vector");
            PlayerPrefs.SetInt("HighScore", tracked_score.score);   //setting it if there are any
        }
    }

    /**
     * comparaPassword: Takes the displayed text and compares it to the password list
     * parameter: is the category number - 0 = weak 1 = strong 2 = veryStrong
     * */
    public void comparePassword(int category)
    {
        if (category == 0)
        {
            if (passwords.getWeakPass().Contains(displayedText.text))
            {
                tracked_score.addScore(scoreAmount);
                Debug.Log("Weak button pressed and diplayed text is matched");
            }
            else
                tracked_score.minusLives();
            //Weak pop-up should be here
            Debug.Log("Weak button pressed but NO match");
        }
        else if (category == 1)
        {
            if (passwords.getModeratePass().Contains(displayedText.text))
            {
                tracked_score.addScore(scoreAmount);
                Debug.Log("Strong button pressed and diplayed text is matched");
            }
            else
                tracked_score.minusLives();
            //Moderate pop-up should be here
            Debug.Log("Strong button pressed but NO match");
        }
        else if (category == 2)
        {
            if (passwords.getStrongPass().Contains(displayedText.text))
            {
                tracked_score.addScore(scoreAmount);
                Debug.Log("Very Strong button pressed and diplayed text is matched");
            }
            else
                tracked_score.minusLives();
            //Strong pop-up should be here
            Debug.Log("Very Strong button pressed but NO match");
        }
    }

    //randomize the passwords that will display on the password text
    public void nextPassword()
    {
        int arrayPosition = rand.Next(0, 16);
        int nextArray = 0;

        if (arrayPosition == nextArray) //making sure that the randomized process doesn't repeat the same arrayposition
        {
            arrayPosition = rand.Next(0, 16);
            displayedText.text = passwords.getPassword(arrayPosition);
        }
        else if (arrayPosition != nextArray)
        {
            nextArray = arrayPosition;
            displayedText.text = passwords.getPassword(arrayPosition);
        }
    }
}
