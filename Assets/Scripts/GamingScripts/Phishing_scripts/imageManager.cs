﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class imageManager : GenericWindow {

    public Image image_component;   //get the reference to the image component for changing
    public score_tracker tracked_score; //get the reference to the score_tracker script - to handle the scoring process
    public int score_amount;    //the amount awarded to the player when they get it right
    public Sprite[] images; //store array of images
    private int counter;    //control which image will appear
    public static System.Random rand = new System.Random(); //randomize the images to be shown

    protected override void Awake()
    {
        this.open();
        tracked_score.reset();  //to reset the life so that it won't game over straight away
        nextPhish();
    }

    void Update()
    {
        if (tracked_score.getLives() <= 0)
        {
            manager.Open(1);    //open the gameOver screen
        }
    }

    private void OnDisable()
    {
        if (PlayerPrefs.GetInt("HighScore", 0) < tracked_score.score)
        {
            PlayerPrefs.SetString("Player", "Vector");
            PlayerPrefs.SetInt("HighScore", tracked_score.score);   //setting it if there are any
        }
    }

    public void checkPhishing(int index)
    {
        if(index == 0)
        {
            if (counter <= 5)  //all the legit screenshots
            {
                tracked_score.addScore(score_amount);
                nextPhish();
                Debug.Log("It is Legit");
            }
            else
            {
                Debug.Log("Wrong answer: It's phishing");
                tracked_score.minusLives();
                nextPhish();
            }
                
        }
        else if ( index == 1)  //includes all the phishing screenshots
        {
            if (counter > 5)
            {
                Debug.Log("It is Phishing");
                tracked_score.addScore(score_amount);
                nextPhish();
            }
            else
            {
                Debug.Log("Wrong answer: It's Legit");
                tracked_score.minusLives();
                nextPhish();
            }
        }
    }

    public void nextPhish()
    {
        if (counter == rand.Next(0, 13))
        {
            counter = rand.Next(0, 13);
            image_component.sprite = images[counter];
        }
        else
        {
            counter = rand.Next(0, 13);
            image_component.sprite = images[counter];
        }
    }

}
