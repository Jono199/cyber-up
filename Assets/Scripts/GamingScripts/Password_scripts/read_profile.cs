﻿using UnityEngine.UI;
using UnityEngine;

public class read_profile : MonoBehaviour {

    struct Profile  //basically the format of the profiles
    {
        public string name;
        public string dob;
        public string address;
        public string phone;
        public string emNumber;
        public string carNumber;

    }

    struct Facebook
    {
        public string details;
        public string onlineName;
        public string home;
        public string hobbies;
        public string engaged;
        public string partner;
        public string petName;
    }
    struct LinkedIn
    {
        public string summary;
        public string education;
        public string job;
    }

    /**
     * 
     * <summary>
     * profiles: hold the file being read
     * placeHolder: to hold all the text being sent through from the text file
     * Reg: creating  profile that can be accessed and displayed.
     * </summary>
     * 
     */
    Profile basicInfo;
    Facebook fb;
    LinkedIn linked;
    public TextAsset profiles;
    public Text displayedText;
    public static string[] placeHolder;

    public void clearText()
    {
       displayedText.text = "";
    }
	// Use this for initialization
	void Start () {

        clearText();
        placeHolder = profiles.text.Split('\n');

        //getting basic information
        basicInfo.name = placeHolder[0];
        basicInfo.dob = placeHolder[1];
        basicInfo.address = placeHolder[2];
        basicInfo.phone = placeHolder[3];
        basicInfo.emNumber = placeHolder[4];
        basicInfo.carNumber = placeHolder[5];

        //getting facebook information
        fb.details = placeHolder[6];
        fb.onlineName = placeHolder[7];
        fb.home = placeHolder[8];
        fb.hobbies = placeHolder[9];
        fb.engaged = placeHolder[10];
        fb.partner = placeHolder[11];
        fb.petName = placeHolder[12];

        //getting linkedIn information
        linked.summary = placeHolder[13];
        linked.education = placeHolder[14];
        linked.job = placeHolder[15];

	}

    public void viewProfile(int whichInfo)
    {
        //hold the string that will be displayed
        string info;  
        string facebook;
        string linkedIn;

        //format to show
        info = basicInfo.name+"\n"+basicInfo.dob+"\n"+basicInfo.address+"\n"+basicInfo.phone+"\n"+basicInfo.emNumber+"\n"+basicInfo.carNumber;
        facebook = fb.details + "\n" + fb.onlineName + "\n" + fb.home + "\n" +fb.hobbies+"\n"+fb.engaged+"\n"+fb.partner+"\n"+fb.petName;
        linkedIn = linked.summary+"\n"+ linked.education + "\n" + linked.job;

        //if statement to see which profile needs to be viewed
        if(whichInfo == 0)
        {
            displayedText.text = info;
        }
        else if(whichInfo == 1)
        {
            displayedText.text = facebook;
        }
        else if(whichInfo == 2)
        {
            displayedText.text = linkedIn;
        }
    }
}