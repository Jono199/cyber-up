﻿using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class read_password : MonoBehaviour {

    public TextAsset textFile;              //will hold the text file that is to be read
    private List<string> weakPassword;      //will hold the list of passwords being filtered into their categories
    private List<string> moderate;    //will hold the list of strong passwords
    private List<string> strong;   //will hold the list of very strong passwords
    private List<string> individualPasswords;           //will hold the weak password
    private string[] allPassword;           //all the passwords, not being filtered and categorised
    

    void Awake () {
        
        //initializing the variables
        weakPassword = new List<string>();
        moderate = new List<string>();
        strong = new List<string>();
        individualPasswords = new List<string>();
        allPassword = textFile.text.Split('\n');    //cutting the text into 3 different parts. weak/strong/Very strong

        for (int i = 0; i < allPassword.Length; i++)    //initializing them into the list for easy access
        {
            individualPasswords.AddRange(allPassword[i].Split(','));
            weakPassword.AddRange(allPassword[0].Split(','));
            moderate.AddRange(allPassword[1].Split(','));
            strong.AddRange(allPassword[2].Split(','));

        }
    }

    //get indivitual password from any categories for the purpose of randomizing it
    public string getPassword(int index)
    {
        return individualPasswords[index];
    }

    //getting all CATEGORIZED passwords for comparing purposes on other classes
    public List<string> getWeakPass()
    {
        return weakPassword;
    }

    public List<string> getModeratePass()
    {
        return moderate;
    }

    public List<string> getStrongPass()
    {
        return strong;
    }

}
