﻿using UnityEngine.UI;
using UnityEngine;

public class score_tracker : MonoBehaviour {

    
    public int score;    //keep track of the score
    public Text scoreDisplay;   //the text field that will show the score
    public Slider sliderRef;    //slider that will display as the healthbar
    private static int lives = 50; //initial health bar value

    //setting up the life values
    void Start()
    {
        sliderRef.minValue = 0;
        sliderRef.maxValue = lives;
        sliderRef.wholeNumbers = true;
        sliderRef.value = lives;
    }
    

    //private float seconds = 0f;   //the time where it will reset
    //private float alpha = 0f;     //time of transition

    // Update is called once per frame
    void Update () {
        scoreDisplay.text = "Score: " + score.ToString();
        sliderRef.value = lives;
    }


    public void addScore(int quantity)
    {
        score += quantity;
        
    }
    //minus the life when user gets it wrong
    public void minusLives()
    {
        lives -= 10;
    }
    //this will reset the lives and score if the user chooses to play a new game
    public void reset()
    {
        lives = 50;
        score = 0;
        this.Start();   //it is to update the slider so that it is reset and shows the green slider
    }
    public int getLives()
    {
        return lives;
    }
}
